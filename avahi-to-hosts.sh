#!/bin/bash

[[ -z "$1" ]] || { echo "$1"|grep -q -- '--repeat'||TARGET="$1" ;};
[[ -z "$TARGET" ]] && TARGET="/etc/hosts.mdns"

avahi_tohosts() { 
[[ -z "$TARGET" ]] || {
    avahires=$(avahi-browse --parsable -at --ignore-local 2>/dev/shm/erros.avahi-browse.log |grep -v 127.0.0.1|grep -v "^.;lo;"|grep -v ";local$"|sed 's/.\+;local;//g'|cut -d";" -f1,2|sed 's/^\(.\+\);\(.\+\)/\2 \1/g'|sort -u );
    echo "$avahires" > "$TARGET" ; } ; } ; 

#echo "$avahires"|wc -l |grep -q ^0$ || 

echo "$@" |grep -- '--repeat' && (
    while (true);do avahi_tohosts;sleep 60;done
)

echo "$@" |grep -q -- '--repeat' || (
    avahi_tohosts
)
