# avahi-browse (mdns) to Hostfile converter


## what

a script using avahi.browse to make this hosts available via a separate hosts file 

## why
since libnss-mdns and `/etc/nsswitch.conf` may not be available ( alpine )
or just too large ( you could share a volume between containers and let this only run in one instance )

## where

if you do not pass the targetfile as argument, `/etc/hosts.mdns` is the default host file generated

## with



## how

**be sure to use mdns-capable networks  ( e.g. weave for docker ) , for docker overlay networks just switch do `endpoint_mode: dnsrr` and do NOT se this approach**


dns is resolved via separate hosts file
you might use it in the following ways

## dnsmasq (docker)
   `dnsmasq  -f -d --strict-order --no-resolv  --server "127.0.0.11#53" --addn-hosts=/etc/hosts.mdns`
   since docker swarm doesnt allow to publish ports in `dnsrr` mode  and `vip` does not resolve containers on other nodes, you
   might just spin up some avahi-sending program and find your containers via HOSTNAME.local


